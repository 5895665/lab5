import java.util.Scanner;
public class  GCDLoop {
    public static void main(String[] margs) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the first number");
        int a = scanner.nextInt();
        System.out.print("Enter the second number");
        int b = scanner.nextInt();
        while (a < 0 || b < 0) {
            System.out.println("Please enter a positive integer.");
            System.out.print("Enter the first number ");
            a = scanner.nextInt();
            System.out.print("Enter the second number");
            b = scanner.nextInt();

        }

        System.out.println("GCD(" + a + "," + b + ")" + "=" + obeb(a,b));
    }

    public static int obeb(int a, int b){

        int remaining = a % b;
        while (remaining != 0) {
            a = b;
            b = remaining;
            remaining = a % b;
        }
        return b;
    }
}
