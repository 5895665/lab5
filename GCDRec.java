import java.util.Scanner;
public class GCDRec {
    public static void main(String[] margs) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("birinci sayıyı giriniz ");
        int a = scanner.nextInt();
        System.out.print("ikinci sayıyı giriniz ");
        int b = scanner.nextInt();

        
        System.out.println("GCD("+a+","+b+")" + "="+ obeb(a,b));
    }

    static int obeb(int a, int b){
        if (b == 0){
            return a;
        }
        int remaining = a % b;
        if (remaining == 0) {
            return b;
        }
        return obeb(b, remaining);
    }
}
